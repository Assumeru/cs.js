import React from 'react';
import DataFiles from './windows/DataFiles';
import Dialogue from './windows/Dialogue';
import Factions from './windows/Factions';
import { useNavigation } from './Navigation';
import { useFactions } from '../hooks/factions';

export default function Content() {
	const page = useNavigation();
	const { overrideFaction, setReaction, factions } = useFactions();
	if(page.page === 'files') {
		return <DataFiles />;
	} else if(page === 'dialogue') {
		return <Dialogue />;
	}
	return <>
		{page.page === 'files' && <DataFiles />}
		{page.page === 'factions' && <Factions overrideFaction={overrideFaction} setReaction={setReaction} factions={factions} />}
		{page.dialogue.map(window => {
			return <Dialogue key={window.id} visible={page.page === window.id} />;
		})}
	</>;
}

import React, { useState, useContext } from 'react';
import Save from './Save';
import '../css/navigation.css';

const Context = React.createContext();
let windowCounter = 0;

function NavItem({ id, children, closeable = false }) {
	const [{ page }, setPage] = useContext(Context);
	return <li className={['nav-item', page === id ? 'active' : ''].filter(Boolean).join(' ')}>
		<a href="" onClick={e => {
			e.preventDefault();
			setPage(prev => ({ ...prev, page: id }));
		}}>
			{children}
		</a>
		{closeable && <button onClick={e => {
			e.preventDefault();
			setPage(prev => ({ ...prev, dialogue: prev.dialogue.filter(t => t.id !== id) }));
		}}>-</button>}
	</li>;
}

function getId(id) {
	return `dialogue_${id}`;
}

export default function Navigation() {
	const [{ dialogue }, setPage] = useContext(Context);
	return <div id="navigation">
		<ul>
			<NavItem id="files">Data Files</NavItem>
			{dialogue.map(topic => {
				return <NavItem id={topic.id} key={topic.id} closeable={dialogue.length > 1}>{topic.name}</NavItem>;
			})}
			<li className="nav-item">
				<button onClick={e => {
					e.preventDefault();
					setPage(prev => {
						const add = { name: 'Dialogue', id: getId(++windowCounter) };
						return { ...prev, dialogue: prev.dialogue.concat(add), page: add.id };
					});
				}}>+</button>
			</li>
			<NavItem id="factions">Factions</NavItem>
			<li className="nav-item"><Save /></li>
		</ul>
	</div>;
}

export function NavigationProvider({ children }) {
	const state = useState({ page: 'files', dialogue: [{ name: 'Dialogue', id: getId(windowCounter) }] });
	return <Context.Provider value={state} children={children} />;
}

export function useNavigation() {
	const [page] = useContext(Context);
	return page;
}

export function useSetDialogueName() {
	const setPage = useContext(Context)[1];
	return name => {
		setPage(prev => {
			if(prev.page.startsWith('dialogue_')) {
				const index = prev.dialogue.findIndex(t => t.id === prev.page);
				if(index >= 0 && prev.dialogue[index].name !== name) {
					const dialogue = prev.dialogue.slice();
					dialogue[index] = { ...dialogue[index], name };
					return { ...prev, dialogue };
				}
			}
			return prev;
		});
	};
}

import React, { useState } from 'react';
import FileList from './files/FileList';
import LoadFiles from './files/LoadFiles';
import { useDataFiles } from '../../hooks/datafiles';
import '../../css/files.css';

export default function DataFiles() {
	const [loading, setLoading] = useState(false);
	const { files, setFiles } = useDataFiles();
	return <div id="data-files-window">
		<h1>Data Files</h1>
		{loading ? <LoadFiles onClose={() => setLoading(false)} onSubmit={res => {
			setFiles(res);
			setLoading(false);
		}} files={files} /> : <FileList setLoading={setLoading} files={files} />}
		<h1>Read me</h1>
		<p>
			This is a browser version of the TES3CS dialogue window.
			It has most of the features of the original, and a number of new ones to ease the editing of PTR section files.
			It also allows faction reaction editing, presenting all factions in a single table and highlighting non-isomorphic values.
		</p>
		<h4>Pros</h4>
		<ul>
			<li>Shows all filters in full</li>
			<li>Bulk editing (Shift+click lines)</li>
			<li>Resizable</li>
			<li>Filterable topic list</li>
			<li>Cannot make dirty edits</li>
			<li>Does not persist every action to disk</li>
			<li>Faction reaction overview</li>
		</ul>
		<h4>Cons</h4>
		<ul>
			<li>No ability to filter by a specific NPC</li>
			<li>Performance suffers drastically from opening multiple tabs</li>
			<li>Cannot operate on ESP/ESM files directly</li>
			<li>Cannot do all the other things TESCS can</li>
		</ul>
		<h1>Usage</h1>
		<p>
			To use this project, you must first load one or more converted ESP/ESM files.
			<a target="_blank" href="https://github.com/Greatness7/tes3conv/releases/tag/v0.0.10">Greatness7's tes3conv</a> must be used to convert all files you wish to load to JSON.
			Once converted, press <code>Load</code>, and then <code>Add File</code>.
			Note that if multiple files are selected in the file browser, they will be sorted by file date before being appended to the list.
			Pressing <code>Save</code> will force a download of the edited JSON file. Use tes3conv to convert it to ESP/ESM as required.
		</p>
		<p>
			<strong>NB:</strong> Just as in TESCS, all files your plugin depends on should also be loaded if you wish to make edits.
			This is not enforced in any way (for ease of viewing) but failure to load all other files may result in "fallen dialogue".
			For example, to edit <code>TR_Mainland.esm</code> the following files should be loaded in this order:<br />
			<code>Morrowind.esm</code>, <code>Tribunal.esm</code>, <code>Bloodmoon.esm</code>, <code>Tamriel_Data.esm</code>,
			and finally <code>TR_Mainland.esm</code>.
		</p>
	</div>;
}

import React, { memo, useMemo, useState } from 'react';
import TopicList from './dialogue/TopicList';
import TopicWindow from './dialogue/TopicWindow';
import { useSetDialogueName } from '../Navigation';
import { useTopics } from '../../hooks/dialogue';
import '../../css/dialogue.css';

const EMPTY_SELECTED = { type: 'Topic', topic: null };
const NO_RECORDS = [];

function Dialogue({ visible }) {
	const [selected, setSelected] = useState(EMPTY_SELECTED);
	const { types, current, setCurrent, deleteRecords, addTopic } = useTopics(selected);
	const setName = useSetDialogueName();
	const { setCategory, setTopic } = useMemo(() => {
		return {
			setCategory: type => {
				setName(type);
				setSelected({ type, topic: null });
			},
			setTopic: topic => {
				setName(topic);
				setSelected(prev => ({ ...prev, topic }));
			}
		};
	}, [setSelected]);
	return <div className="dialogue-window" hidden={!visible}>
		<TopicList category={selected.type} topic={selected.topic} types={types} setCategory={setCategory} setTopic={setTopic} addTopic={addTopic} />
		<TopicWindow type={selected.type} records={current ?? NO_RECORDS} setRecords={setCurrent} inTopic={current !== null} deleteRecords={deleteRecords} visible={visible} />
	</div>;
}

export default memo(Dialogue);

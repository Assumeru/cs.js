import React, { memo } from 'react';
import Reaction from './factions/Reaction';
import '../../css/factions.css';

function Factions({ overrideFaction, setReaction, factions }) {
	const ids = Object.keys(factions);
	return <div className="faction-window">
		<table className="reaction-table">
			<thead>
				<tr>
					<td>From \ To</td>
					{ids.map(id => {
						const recordId = factions[id]?.record?.id ?? id;
						const deprecated = factions[id]?.deprecated;
						return <th key={id} colSpan={2} className={deprecated ? 'deprecated' : undefined}>{recordId}</th>;
					})}
				</tr>
			</thead>
			<tbody>
				{ids.map(id => {
					const { record, file, deprecated } = factions[id];
					const readOnly = !file.active;
					return <tr key={id}>
						<th>
							<i title={file.name} style={{ backgroundColor: file.color }} className="file-color" />{' '}
							<span className={deprecated ? 'deprecated' : undefined}>{record.id}</span>{' '}
							{readOnly && <button onClick={() => overrideFaction(id)}>Override</button>}
						</th>
						{ids.map(otherId => {
							return <Reaction key={otherId} id={id} otherId={otherId} faction={record} otherFaction={factions[otherId]?.record} readOnly={readOnly} changeReaction={setReaction} />;
						})}
					</tr>;
				})}
			</tbody>
		</table>
	</div>;
}

export default memo(Factions);

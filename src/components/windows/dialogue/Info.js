import React from 'react';
import Filter from './Filter';

export const INFO_COLUMNS = 17;

export function InfoHeader() {
	return <tr>
		<th title="File">F</th>
		<th title="Deleted">D</th>
		<th>ID</th>
		<th>Text</th>
		<th title="Disposition">D</th>
		<th>Speaker</th>
		<th>Race</th>
		<th>Sex</th>
		<th>Class</th>
		<th colSpan={2}>Faction</th>
		<th>Cell</th>
		<th colSpan={2}>Player faction</th>
		<th colSpan={3}>Filters</th>
	</tr>;
}

function getSex(sex) {
	if(sex === 'Male') {
		return 'M';
	} else if(sex === 'Female') {
		return 'F';
	}
	return '';
}

function getFaction(faction) {
	if(faction === 'FFFF') {
		return '[None]';
	}
	return faction;
}

function getRank(rank) {
	if(rank === -1) {
		return '';
	}
	return rank;
}

export function Info({ record, file, onClick, selected }) {
	const className = selected ? 'selected' : undefined;
	return <>
		<tr onClick={onClick} className={className}>
			<td rowSpan={2}>
				<i title={file.name} style={{ backgroundColor: file.color }} className="file-color" />
			</td>
			<td rowSpan={2}>{'deleted' in record && '*'}</td>
			<td rowSpan={2} className="select-all">{record.info_id}</td>
			<td rowSpan={2}>{record.text}</td>
			<td rowSpan={2} className="text-right">{record.data?.disposition}</td>
			<td rowSpan={2}>{record.speaker_id}</td>
			<td rowSpan={2}>{record.speaker_rank}</td>
			<td rowSpan={2}>{getSex(record.data?.speaker_sex)}</td>
			<td rowSpan={2}>{record.speaker_class}</td>
			<td rowSpan={2}>{getFaction(record.speaker_faction)}</td>
			<td rowSpan={2} className="text-right">{getRank(record.data?.speaker_rank)}</td>
			<td rowSpan={2}>{record.speaker_cell}</td>
			<td rowSpan={2}>{record.player_faction}</td>
			<td rowSpan={2} className="text-right">{getRank(record.data?.player_rank)}</td>
			{['0', '1', '2'].map(i => {
				return <Filter key={i} filter={record.filters?.find(f => f.slot === 'Slot' + i)} />;
			})}
		</tr>
		<tr onClick={onClick} className={className}>
			{['3', '4', '5'].map(i => {
				return <Filter key={i} filter={record.filters?.find(f => f.slot === 'Slot' + i)} />;
			})}
		</tr>
	</>;
}

import React, { memo } from 'react';
import { Info, InfoHeader, INFO_COLUMNS } from './Info';
import { Journal, JournalHeader, JOURNAL_COLUMNS } from './Journal';

export function Header({ type }) {
	const Elem = type === 'Journal' ? JournalHeader : InfoHeader;
	return <thead><Elem /></thead>;
}

export function getColumns(type) {
	if(type === 'Journal') {
		return JOURNAL_COLUMNS;
	}
	return INFO_COLUMNS;
}

function InfoRow({ type, record, file, selected, index, setRecords }) {
	const Record = type === 'Journal' ? Journal : Info;
	return <Record record={record} file={file} selected={selected} onClick={e => {
		setRecords(prev => {
			let lowerBound = index;
			let upperBound = index;
			if(e.shiftKey) {
				let firstIndex = Infinity;
				let lastIndex = -Infinity;
				for(let i = 0; i < prev.length; i++) {
					if(prev[i].selected) {
						firstIndex = Math.min(i, firstIndex);
						lastIndex = Math.max(i, lastIndex);
					} else if(Number.isFinite(lastIndex)) {
						break;
					}
				}
				lowerBound = Math.min(firstIndex, lowerBound);
				upperBound = Math.max(lastIndex, upperBound);
				e.preventDefault();
			}
			return prev.map((r, i) => {
				if(i >= lowerBound && i <= upperBound) {
					return { ...r, selected: true };
				} else if(r.selected) {
					return { ...r, selected: false };
				}
				return r;
			});
		});
	}} />;
}

export default memo(InfoRow);

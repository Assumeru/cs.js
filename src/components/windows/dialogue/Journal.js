import React from 'react';

export const JOURNAL_COLUMNS = 8;

export function JournalHeader() {
	return <tr>
		<th title="File">F</th>
		<th title="Deleted">D</th>
		<th>ID</th>
		<th>Text</th>
		<th className="text-right">Index</th>
		<th>Name</th>
		<th>Finished</th>
		<th>Restart</th>
	</tr>;
}

export function Journal({ record, file, onClick, selected }) {
	const className = selected ? 'selected' : undefined;
	return <tr onClick={onClick} className={className}>
		<td>
			<i title={file.name} style={{ backgroundColor: file.color }} className="file-color" />
		</td>
		<td>{'deleted' in record && '*'}</td>
		<td className="select-all">{record.info_id}</td>
		<td>{record.text}</td>
		<td className="text-right">{record.data?.disposition}</td>
		<td className="text-center"><input type="checkbox" checked={!!record.quest_name} disabled /></td>
		<td className="text-center"><input type="checkbox" checked={!!record.quest_finish} disabled /></td>
		<td className="text-center"><input type="checkbox" checked={!!record.quest_restart} disabled /></td>
	</tr>;
}

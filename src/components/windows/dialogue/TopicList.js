import React, { memo, useState } from 'react';

function nameTaken(name, types) {
	if(!name) {
		return true;
	}
	const lower = name.toLowerCase();
	for(const category in types) {
		if(types[category]?.some(t => t[0] === lower)) {
			return true;
		}
	}
	return false;
}

function TopicList({ category, setCategory, types, topic, setTopic, addTopic }) {
	const [search, setSearch] = useState('');
	const [name, setName] = useState('');
	const selected = types[category] ?? null;
	const lowerSearch = search.toLowerCase();
	return <div className="topic-list">
		<div className="categories">
			{Object.keys(types).map(type => {
				return <button key={type} onClick={e => {
					e.preventDefault();
					setCategory(type);
				}} className={type === category ? 'active' : undefined}>{type}</button>;
			})}
		</div>
		<input type="search" placeholder="Filter" value={search} onChange={e => setSearch(e.target.value)} />
		{selected && <div className="topics">
			<ol>
				{selected.map(([key, value]) => {
					if(lowerSearch && !key.includes(lowerSearch)) {
						return null;
					}
					return <li key={key} className={key === topic ? 'active' : undefined} onClick={() => setTopic(key)}>{value}</li>;
				})}
			</ol>
		</div>}
		{['Journal', 'Topic'].includes(category) && selected && <form onSubmit={e => {
			e.preventDefault();
			addTopic(category, name);
			setTopic(name.toLowerCase());
			setName('');
		}}>
			<input type="text" placeholder="New topic" value={name} onChange={e => setName(e.target.value)} />
			<input type="submit" value="Add" disabled={nameTaken(name, types)} />
		</form>}
	</div>;
}

export default memo(TopicList);

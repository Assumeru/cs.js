import React, { useState } from 'react';

const JUMP = 10;

function getDeleted(records, readOnly) {
	let deleted = false;
	let undeleted = false;
	if(!readOnly) {
		for(const { record } of records) {
			const del = 'deleted' in record;
			deleted ||= del;
			undeleted ||= !del;
			if(deleted && undeleted) {
				break;
			}
		}
	}
	return [deleted, undeleted];
}

export default function EditorControls({ records, set, deleteRecords, addLine, moveSelection, firstSelected, lastSelected, overrideSelection, readOnly = false }) {
	const [id, setId] = useState('');
	const [deleted, undeleted] = getDeleted(records, readOnly);
	const move = amount => e => {
		e.preventDefault();
		moveSelection(amount);
	};
	return <div className="editor-controls">
		<button disabled={!undeleted} onClick={e => {
			e.preventDefault();
			set('deleted', 0);
		}}>Mark deleted</button>{' '}
		<button disabled={!deleted} onClick={e => {
			e.preventDefault();
			set('deleted', undefined);
		}}>Undelete</button>
		<span className="spacer"> | </span>
		<button disabled={!readOnly} onClick={e => {
			e.preventDefault();
			overrideSelection();
		}}>Override</button>{' '}
		<button disabled={readOnly} onClick={e => {
			e.preventDefault();
			if(window.confirm('Are you sure?')) {
				deleteRecords(records);
			}
		}}>Delete</button>
		<span className="spacer"> | </span>
		<button disabled={firstSelected || readOnly} onClick={move(-JUMP)}>&uarr;&times;{JUMP}</button>{' '}
		<button disabled={firstSelected || readOnly} onClick={move(-1)}>&uarr;</button>{' '}
		<button disabled={lastSelected || readOnly} onClick={move(1)}>&darr;</button>{' '}
		<button disabled={lastSelected || readOnly} onClick={move(JUMP)}>&darr;&times;{JUMP}</button>{' '}
		<form onSubmit={e => {
			e.preventDefault();
			moveSelection(id);
			setId('');
		}}>
			Move below <input type="text" placeholder="ID" value={id} onChange={e => setId(e.target.value)} readOnly={readOnly} />
			{' '}<input type="submit" value="Move" disabled={!id} disabled={readOnly} />
		</form>
		<span className="spacer"> | </span>
		<button onClick={e => {
			e.preventDefault();
			addLine(true);
		}}>Add line above</button>{' '}
		<button onClick={e => {
			e.preventDefault();
			addLine(false);
		}}>Add line below</button>{' '}
		<button onClick={e => {
			e.preventDefault();
			addLine(true, records);
		}}>Duplicate above</button>{' '}
		<button onClick={e => {
			e.preventDefault();
			addLine(false, records);
		}}>Duplicate below</button>
	</div>;
}

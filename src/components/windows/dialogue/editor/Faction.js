import React from 'react';
import { getValues, MULTIPLE } from './RecordInput';

const RANKS = 10;

function getRanks(names) {
	const out = [];
	for(let i = 0; i < RANKS; i++) {
		out.push(<option key={i} value={i}>{names[i] ?? `Rank ${i}`}</option>);
	}
	return out;
}

export function FactionRank({ records, set, name, ranks, readOnly = false }) {
	const value = getValues(records, name, true, -1);
	const onChange = e => set(name, +e.target.value, true);
	return <select value={value} onChange={onChange} disabled={readOnly}>
		{value === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
		<option value="-1"></option>
		{getRanks(ranks ?? [])}
	</select>;
}

export default function Faction({ value, set, name, factions, none = false, readOnly = false }) {
	const onChange = e => set(name, e.target.value);
	return <select value={value.id || value} onChange={onChange} disabled={readOnly}>
		{value === MULTIPLE && <option value={MULTIPLE} disabled>[Multiple]</option>}
		<option value=""></option>
		{none && <option value="FFFF">[No Faction]</option>}
		{factions.map(faction => <option key={faction.id} value={faction.id}>{faction.id}</option>)}
	</select>;
}

import React from 'react';
import RecordInput, { getValue } from './RecordInput';
import EditorControls from './EditorControls';

function Checkbox({ records, set, name, readOnly }) {
	const multiple = records.length > 1;
	const value = multiple ? false : getValue(records[0], name);
	return <input type="checkbox" checked={!!value} onChange={e => set(name, e.target.checked ? 1 : undefined)} disabled={multiple || readOnly} />;
}

export default function TopicEditor({ records, set, deleteRecords, addLine, moveSelection, firstSelected, lastSelected, overrideSelection, readOnly = false }) {
	const multiple = records.length > 1;
	return <div className="editor">
		<div className="editor-row">
			<div className="conditions journal">
				<table>
					<tbody>
						<tr>
							<th>Quest Name</th>
							<td><Checkbox records={records} set={set} name="quest_name" readOnly={readOnly} /></td>
							<th>Finished</th>
							<td><Checkbox records={records} set={set} name="quest_finish" readOnly={readOnly} /></td>
							<th>Restart</th>
							<td><Checkbox records={records} set={set} name="quest_restart" readOnly={readOnly} /></td>
							<th>Index</th>
							<td><RecordInput records={records} set={set} name="disposition" data readOnly={readOnly} /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<EditorControls
			records={records}
			set={set}
			readOnly={readOnly}
			deleteRecords={deleteRecords}
			overrideSelection={overrideSelection}
			addLine={addLine}
			moveSelection={moveSelection}
			firstSelected={firstSelected}
			lastSelected={lastSelected} />
		<div>
			<RecordInput records={records} set={set} name="text" type="textarea" maxLength={512} disabled={multiple} readOnly={readOnly} />
		</div>
	</div>;
}

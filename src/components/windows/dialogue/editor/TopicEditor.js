import React from 'react';
import FilterEditor from './FilterEditor';
import RecordInput, { getValues, MULTIPLE } from './RecordInput';
import Faction, { FactionRank } from './Faction';
import EditorControls from './EditorControls';
import { useActors, useCells, useClasses, useFactions, useRaces } from '../../../../hooks/dialogue';

function getFaction(factions, records, name) {
	const id = getValues(records, name);
	if(id === MULTIPLE || !id || id === 'FFFF') {
		return id;
	}
	const lower = id.toLowerCase();
	return factions.find(f => f.id === id || f.id.toLowerCase() === lower);
}

export default function TopicEditor({ records, set, setFilter, type, deleteRecords, addLine, moveSelection, firstSelected, lastSelected, overrideSelection, readOnly = false }) {
	const factions = useFactions();
	const speakerFaction = getFaction(factions, records, 'speaker_faction');
	const playerFaction = getFaction(factions, records, 'player_faction');
	return <div className="editor">
		<div>
			<RecordInput records={records} set={set} name="text" type="textarea" maxLength={512} readOnly={readOnly} />
		</div>
		<EditorControls
			records={records}
			set={set}
			readOnly={readOnly}
			deleteRecords={deleteRecords}
			overrideSelection={overrideSelection}
			addLine={addLine}
			moveSelection={moveSelection}
			firstSelected={firstSelected}
			lastSelected={lastSelected} />
		<div className="editor-row">
			<div className="conditions">
				<table>
					<tbody>
						<tr>
							<th>ID</th>
							<td><RecordInput records={records} set={set} name="speaker_id" use={useActors} readOnly={readOnly} /></td>
							<th>Sex</th>
							<td>
								<RecordInput type="select" records={records} set={set} name="speaker_sex" data readOnly={readOnly}>
									<option value=""></option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</RecordInput>
							</td>
							<th>Disposition</th>
							<td><RecordInput records={records} set={set} name="disposition" data def={0} readOnly={readOnly} /></td>
						</tr>
						<tr>
							<th>Race</th>
							<td><RecordInput records={records} set={set} name="speaker_rank" use={useRaces} readOnly={readOnly} /></td>
							<th colSpan={4}>Function/Variable</th>
						</tr>
						<tr>
							<th>Class</th>
							<td><RecordInput records={records} set={set} name="speaker_class" use={useClasses} readOnly={readOnly} /></td>
							<FilterEditor slot="0" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						<tr>
							<th>Faction</th>
							<td><Faction value={speakerFaction} set={set} name="speaker_faction" factions={factions} none readOnly={readOnly} /></td>
							<FilterEditor slot="1" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						<tr>
							<th>Rank</th>
							<td><FactionRank records={records} set={set} name="speaker_rank" ranks={speakerFaction?.ranks} readOnly={readOnly} /></td>
							<FilterEditor slot="2" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						<tr>
							<th>Cell</th>
							<td><RecordInput records={records} set={set} name="speaker_cell" use={useCells} readOnly={readOnly} /></td>
							<FilterEditor slot="3" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						<tr>
							<th>PC Faction</th>
							<td><Faction value={playerFaction} set={set} name="player_faction" factions={factions} readOnly={readOnly} /></td>
							<FilterEditor slot="4" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						<tr>
							<th>PC Rank</th>
							<td><FactionRank records={records} set={set} name="player_rank" ranks={playerFaction?.ranks} readOnly={readOnly} /></td>
							<FilterEditor slot="5" records={records} set={setFilter} readOnly={readOnly} />
						</tr>
						{type === 'Voice' && <tr>
							<th>Sound</th>
							<td colSpan={5}><RecordInput records={records} set={set} name="sound_path" maxLength={31} readOnly={readOnly} /></td>
						</tr>}
					</tbody>
				</table>
			</div>
			<div className="script">
				<RecordInput records={records} set={set} name="result" type="textarea" maxLength={30000} readOnly={readOnly} />
			</div>
		</div>
	</div>;
}

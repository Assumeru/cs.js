import React, { memo } from 'react';

function Reaction({ otherId, faction, otherFaction, readOnly, changeReaction }) {
	const reaction = faction.reactions.find(r => r.faction === otherId);
	const otherReaction = otherFaction?.reactions?.find(r => r.faction === faction.id);
	const classes = ['text-right'];
	if((reaction?.reaction ?? 0) !== (otherReaction?.reaction ?? 0)) {
		classes.push('different-reaction');
	} else if(reaction?.reaction) {
		classes.push('same-reaction');
	}
	return <>
		<td>
			<input type="number" value={reaction?.reaction ?? 0} disabled={readOnly} min={-10} max={10} onChange={e => changeReaction(faction.id, otherId, +e.target.value)} />
		</td>
		<td className={classes.join(' ')}>
			{otherReaction?.reaction ?? '0'}
		</td>
	</>;
}

export default memo(Reaction);

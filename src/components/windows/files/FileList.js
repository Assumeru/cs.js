import React from 'react';

export default function FileList({ setLoading, files }) {
	return <>
		<ol>
			{files.map((file, i) => {
				return <li key={i}>
					<i className="file-color" style={{ backgroundColor: file.color }} /> {file.name}
				</li>;
			})}
		</ol>
		<button onClick={e => {
			e.preventDefault();
			setLoading(true);
		}}>Load</button>
	</>;
}

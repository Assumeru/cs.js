import React, { useContext, useEffect, useState } from 'react';

const Context = React.createContext();

const NO_OP = () => undefined;
const EMPTY_STATE = { files: [], setFiles: NO_OP, initialActive: null, active: null, setActive: NO_OP };
export const backup = { active: null };

function update(setState, key, onChange = NO_OP) {
	return value => setState(prev => {
		if(typeof value === 'function') {
			value = value(prev[key]);
		}
		if(value === prev[key]) {
			return prev;
		}
		const out = { ...prev, [key]: value };
		onChange(out);
		backup.active = out.active;
		return out;
	});
}

export function DataFilesProvider({ children }) {
	const [state, setState] = useState(EMPTY_STATE);
	useEffect(() => {
		state.setFiles = update(setState, 'files', newState => {
			const activeIndex = newState.files.length - 1;
			newState.files = newState.files.map((file, i) => {
				if(i === activeIndex) {
					if(!file.active) {
						return { ...file, active: true };
					}
				} else if(file.active) {
					return { ...file, active: false };
				}
				return file;
			});
			newState.active = newState.files[activeIndex] ?? null;
			newState.initialActive = newState.active;
		});
		state.setActive = update(setState, 'active');
	}, []);
	return <Context.Provider value={state} children={children} />;
}

export function useDataFiles() {
	return useContext(Context);
}

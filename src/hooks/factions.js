'use strict';

import { useEffect, useMemo, useState } from 'react';
import { useCached } from './cache';
import { useDataFiles } from './datafiles';
import { FLAG_IGNORED } from '../constants';

const EMPTY_STATE = {};

function filterFactions(records) {
	return records.reduce((factions, record) => {
		if(record.flags?.[1] & FLAG_IGNORED) {
			return factions;
		} else if(record.type !== 'Faction') {
			return factions;
		}
		factions.push(record);
		return factions;
	}, []);
}

function buildMap(cached, files) {
	const factions = {};
	const canonical = {};
	cached.forEach((records, i) => {
		records.forEach(record => {
			const id = record.id?.toLowerCase();
			factions[id] = { record, file: files[i], deprecated: /<Deprecated>/i.test(record.name) };
			canonical[id] = record.id;
		});
	});
	const out = {};
	for(const id in factions) {
		const record = { ...factions[id].record };
		if(!record.reactions) {
			record.reactions = [];
		} else {
			record.reactions = record.reactions.reduce((reactions, reaction) => {
				if(reaction.faction) {
					const otherId = reaction.faction.toLowerCase();
					if(!(otherId in canonical)) {
						canonical[otherId] = reaction.faction;
					}
					const canonicalId = canonical[otherId];
					const prev = reactions.find(r => r.faction === canonicalId);
					if(!prev) {
						reactions.push({ faction: canonicalId, reaction: reaction.reaction ?? 0 });
					} else if(prev.reaction > reaction.reaction) {
						prev.reaction = reaction.reaction;
					}
				}
				return reactions;
			}, []);
		}
		factions[id].record = record;
		out[record.id] = factions[id];
	}
	return out;
}

function isInvalid({ reaction }) {
	return isNaN(reaction) || !reaction;
}

function fixRecord({ record }) {
	const index = record.reactions.findIndex(isInvalid);
	if(index >= 0) {
		const copy = { ...record, reactions: record.reactions.slice(0, index) };
		for(let i = index + 1; i < record.reactions.length; i++) {
			const reaction = record.reactions[i];
			if(!isInvalid(reaction)) {
				copy.reactions.push(reaction);
			}
		}
		return copy;
	}
	return record;
}

function saveFactions(setActive, copy) {
	const toDo = {};
	for(const id in copy) {
		if(copy[id].file.active) {
			toDo[id] = fixRecord(copy[id]);
		}
	}
	setActive(prev => {
		const json = prev.json.slice();
		let factionStart = -1;
		let factionEnd = -1;
		for(let i = 0; i < json.length; i++) {
			const record = json[i];
			if(record.type === 'Faction') {
				if(factionStart < 0) {
					factionStart = i;
				}
				if(record.id in toDo) {
					json[i] = toDo[record.id];
					delete toDo[record.id];
				}
			} else if(factionStart >= 0) {
				factionEnd = i;
			}
		}
		if(factionEnd < 0) {
			factionEnd = json.length;
		}
		const remaining = Object.values(toDo);
		if(remaining.length) {
			json.splice(factionEnd, 0, ...remaining);
		}
		return { ...prev, json };
	});
}

function createSetters(setFactions, setActive, files) {
	const set = f => {
		setFactions(prev => {
			const copy = f(prev);
			if(copy !== prev) {
				setTimeout(() => saveFactions(setActive, copy), 0);
			}
			return copy;
		});
	};
	return {
		overrideFaction(id) {
			set(prev => {
				if(id in prev) {
					const copy = { ...prev };
					copy[id] = { ...prev[id], file: files[files.length - 1] };
					return copy;
				}
				return prev;
			});
		},
		setReaction(id, otherId, reaction) {
			set(prev => {
				if(id in prev) {
					const copy = { ...prev };
					copy[id] = { ...copy[id], record: { ...copy[id].record, reactions: copy[id].record.reactions.slice() } };
					const index = copy[id].record.reactions.findIndex(r => r.faction === otherId);
					if(index >= 0) {
						copy[id].record.reactions[index] = { ...copy[id].record.reactions[index], reaction };
					} else {
						copy[id].record.reactions.push({ faction: otherId, reaction });
					}
					return copy;
				}
				return prev;
			});
		}
	};
}

export function useFactions() {
	const { files, setActive } = useDataFiles();
	const cached = useCached(files, filterFactions);
	const [factions, setFactions] = useState(EMPTY_STATE);
	useEffect(() => {
		setFactions(buildMap(cached, files));
	}, [cached, files]);
	const { overrideFaction, setReaction } = useMemo(() => createSetters(setFactions, setActive, files), [setFactions, setActive, files]);
	return { overrideFaction, setReaction, factions };
}

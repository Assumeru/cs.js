import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import ErrorPage from './components/ErrorPage';
import { NavigationProvider } from './components/Navigation';
import { DataFilesProvider } from './hooks/datafiles';
import { DialogueProvider } from './hooks/dialogue';
import './css/index.css';

ReactDOM.render(
	<React.StrictMode>
		<ErrorPage>
			<NavigationProvider>
				<DataFilesProvider>
					<DialogueProvider>
						<App />
					</DialogueProvider>
				</DataFilesProvider>
			</NavigationProvider>
		</ErrorPage>
	</React.StrictMode>,
	document.getElementById('root')
);
